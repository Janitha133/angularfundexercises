import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
})
export class ChildComponent implements OnInit {
  @Output() buttonClick = new EventEmitter();
  stopped: boolean = false;
  counter: number = 0;
  intervalId: any;

  constructor() {}

  ngOnInit(): void {
    this.intervalId = setInterval(() => {
      this.counter++;
    }, 1000);
  }

  buttonClicked() {
    this.buttonClick.emit(this.counter);
  }

  stopTimer() {
    clearInterval(this.intervalId);
    this.stopped = true;
  }
}
