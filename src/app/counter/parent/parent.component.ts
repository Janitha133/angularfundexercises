import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss'],
})
export class ParentComponent implements OnInit {
  currentCounter: any;

  constructor() {}

  ngOnInit(): void {}

  handleChildButtonClick(value: any) {
    this.currentCounter = value;
  }
}
