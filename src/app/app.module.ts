import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { EventsAppComponent } from './events-app.component';
import { EventsAddressComponent } from './events/events-address.component';
import { EventsDetailsAppComponent } from './events/events-details.component';
import { ChildComponent } from './counter/child/child.component';
import { ParentComponent } from './counter/parent/parent.component';

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsDetailsAppComponent,
    EventsAddressComponent,
    ChildComponent,
    ParentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }
