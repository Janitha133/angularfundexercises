import { Component, Input } from '@angular/core'

@Component({
  selector: 'events-address',
  templateUrl: './events-address.component.html',
})
export class EventsAddressComponent { 
  @Input() address:any
}